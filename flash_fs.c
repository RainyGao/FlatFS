#include <string.h>
#include "flash_io.h"
#include "flash_fs.h"

void Sys_InitFS(void)
{
    PAGE_CTRL_INFO ctrl;
    
    FlashRead(FS_FLAG_PAGE_START, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
    if (memcmp(ctrl.byte, "FORMAT", 6) != 0)
    {
        FormatFS(); 
        //memcpy(ctrl.byte, "FORMAT", 6);
        //FlashWrite(FS_FLAG_PAGE_START, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
    }
}

void FormatFS(void)
{    
    int i;
    PAGE_CTRL_INFO ctrl;

    for (i = 0; i < FS_FLAG_PAGE_NUM; ++ i)
        FlashErase(FS_FLAG_PAGE_START + i);

    for (i = 0; i < HASH_NODE_PAGE_NUM; ++ i)
        FlashErase(HASH_NODE_PAGE_START + i);

    for (i = 0; i < BITMAP_PAGE_NUM; ++ i)
        FlashErase(BITMAP_PAGE_START + i);

    for (i = 0; i < RECYCLE_PAGE_NUM; ++ i)
        FlashErase(RECYCLE_PAGE_START + i);

    for (i = 0; i < BAD_PAGE_PAGE_NUM; ++ i)
        FlashErase(BAD_PAGE_PAGE_START + i);

    for (i = 0; i < DATA_PAGE_NUM; ++ i)
        FlashErase(DATA_PAGE_START + i);
    
    memcpy(ctrl.byte, "FORMAT", 6);
    FlashWrite(FS_FLAG_PAGE_START, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
}

static void RecyclePage(void)
{
    char bitmap[PAGE_DATA_SIZE], recycle[PAGE_DATA_SIZE];
    int i;

    FlashRead(BITMAP_PAGE_START, sizeof (PAGE_CTRL_INFO), bitmap, PAGE_DATA_SIZE); 
    FlashRead(RECYCLE_PAGE_START, sizeof (PAGE_CTRL_INFO), recycle, PAGE_DATA_SIZE); 

    for (i = 0; i < PAGE_DATA_SIZE; ++ i)
        bitmap[i] |= ~recycle[i];
    FlashWrite(BITMAP_PAGE_START, sizeof (PAGE_CTRL_INFO), bitmap, PAGE_DATA_SIZE);

    FlashErase(RECYCLE_PAGE_START);
}

static int GetFreePage(void)
{
    char bitmap[PAGE_DATA_SIZE];
    int i, times;

    for (times = 0; times < 2; ++ times)
    {
        FlashRead(BITMAP_PAGE_START, sizeof (PAGE_CTRL_INFO), bitmap, PAGE_DATA_SIZE); 
        for (i = 0; i < DATA_PAGE_NUM; ++ i)
        {
            if (bitmap[i >> 3] & (0x1 << (i & 0x7)))
            {
                bitmap[i >> 3] &= ~(0x1 << (i & 0x7));
                FlashWrite(BITMAP_PAGE_START, sizeof (PAGE_CTRL_INFO), (char *) &bitmap, PAGE_DATA_SIZE);

                return i;
            }
        }

        if (times == 0)
            RecyclePage();
    }

    return -1;
}

static void SetRecyclePage(int PageNo)
{
    char recycle[PAGE_DATA_SIZE];
    
    FlashRead(RECYCLE_PAGE_START, sizeof (PAGE_CTRL_INFO), recycle, PAGE_DATA_SIZE); 
    recycle[PageNo >> 3] &= ~(0x1 << (PageNo & 0x7));

    FlashWrite(RECYCLE_PAGE_START, sizeof (PAGE_CTRL_INFO), (char *) &recycle, PAGE_DATA_SIZE);
}

static int GetHashValue(char *pszName)
{
    unsigned int hash = 0;
    int i, len;
    
    len = strlen(pszName);    
    for (i = 0; i < len; ++ i)
        hash = pszName[i] + (hash << 6) + (hash << 16) - hash;

    return (hash & 0x1F);
}

static int GetHashIndex(HASH_NODE hash[])
{
    PAGE_CTRL_INFO ctrl;
    int i;

    FlashRead(HASH_NODE_PAGE_START, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
    
    for (i = 0; i < 8; ++ i)
    {
        if (ctrl.byte[0] & (0x1 << i))
            break;
    }
    if (i >= 8)
        return -1; //bug

    FlashRead(HASH_NODE_PAGE_START + i / 4, 
              sizeof (PAGE_CTRL_INFO) + (i & 0x3) * HASH_INDEX_SIZE, 
              (char *) hash, HASH_INDEX_SIZE);

    return i;
}

static int SetHashIndex(HASH_NODE hash[])
{
    PAGE_CTRL_INFO ctrl;
    HASH_NODE tmp[MAX_HASH_BUCKET];
    int i, idx;
    
    idx = GetHashIndex(tmp);
    if (idx == -1)
        return -1; //bug

    for (i = 0; i < MAX_HASH_BUCKET; ++ i)
    {
        tmp[i].page_no &= hash[i].page_no;
        if (tmp[i].page_no != hash[i].page_no)
            break;
    }
    if (i < MAX_HASH_BUCKET) //需要使用下一个索引
    {
        FlashRead(HASH_NODE_PAGE_START, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
        if (idx < 7)
        {
            ctrl.byte[0] &= ~(0x1 << idx);
            ++ idx;
            FlashWrite(HASH_NODE_PAGE_START, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
        }
        else
        {
            FlashErase(HASH_NODE_PAGE_START);
            FlashErase(HASH_NODE_PAGE_START + 1);
            idx = 0;                        
        }      
    }

    FlashWrite(HASH_NODE_PAGE_START + idx / 4, 
              sizeof (PAGE_CTRL_INFO) + (idx & 0x3) * HASH_INDEX_SIZE, 
              (char *) hash, HASH_INDEX_SIZE);

    return 0;
}

static int FindFile(char *pszName, FILE_DESCRIPTION *file)
{
    HASH_NODE hash[MAX_HASH_BUCKET];
    PAGE_CTRL_INFO ctrl;
    FILE_HEAD head_info;
    unsigned short p, q;
    int value = GetHashValue(pszName);
    int idx;
    
    idx = GetHashIndex(hash);
    if (idx == -1)
        return -1; //bug

    p = hash[value].page_no;
    q = FS_NULL;
    while (p != FS_NULL)
    {
        FlashRead(DATA_PAGE_START + p, sizeof (PAGE_CTRL_INFO), (char *) &head_info, sizeof (FILE_HEAD));
        if (strcmp(head_info.name, pszName) == 0)
            break;
        FlashRead(DATA_PAGE_START + p, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
        q = p;
        p = ctrl.byte[2] * 256 + ctrl.byte[3];
    }
    if (p == FS_NULL) 
        return -1;

    memcpy(&file->head_info, &head_info, sizeof (FILE_HEAD));
    file->pre_hash_page = q;
    file->head_page = p;
    file->cur_page = -1;
    file->cur_offset = -1;
    file->cur_size = file->head_info.size;
    if (file->cur_size == FS_NULL)
        file->cur_size = 0;
    file->offset = 0;

    return 0;
}

int Sys_FS_Open(char *pszName, FILE_DESCRIPTION *file, int flag)
{
    PAGE_CTRL_INFO ctrl;
    HASH_NODE hash[MAX_HASH_BUCKET];
    int value, idx, page;
    
    if (strlen(pszName) > 13)
        return -1;

    if (FindFile(pszName, file) == 0)
        return 0;

    if (flag == FS_RDONLY)
        return -2;
    
    page = GetFreePage();
    if (page == -1)
        return -3;
        
    strcpy(file->head_info.name, pszName);
    file->head_info.size = FS_NULL;
    
    FlashWrite(DATA_PAGE_START + page, sizeof(PAGE_CTRL_INFO), (char *) &file->head_info, sizeof (FILE_HEAD));

    memset(ctrl.byte, 0xFF, sizeof (PAGE_CTRL_INFO));

    idx = GetHashIndex(hash);
    if (idx == -1)
        return -4; //bug
    value = GetHashValue(pszName);

    ctrl.byte[2] = hash[value].page_no / 256;
    ctrl.byte[3] = hash[value].page_no % 256;
    FlashWrite(DATA_PAGE_START + page, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));

    hash[value].page_no = page;
    if (SetHashIndex(hash) == -1)
        return -5;//bug

    file->pre_hash_page = FS_NULL;
    file->head_page = page;
    file->cur_page = -1;
    file->cur_offset = -1;
    file->cur_size = file->head_info.size;
    if (file->cur_size == FS_NULL)
        file->cur_size = 0;
    file->offset = 0;

    return 0;
}

int Sys_FS_Seek(FILE_DESCRIPTION *file, int offset, int origin)
{
    int new_pos;

    switch (origin)
    {
    case FS_SEEK_SET:
        new_pos = offset;
        break;
    case FS_SEEK_CUR:
        new_pos = file->offset + offset;
        break;
    case FS_SEEK_END:
        new_pos = file->cur_size + offset;
        break;
    default:
        return -1;
    }

    if (new_pos < 0)
        return -1;
    file->offset = new_pos;
    file->cur_page = -1;
    file->cur_offset = -1;
    
    return new_pos;
}

static int GetNextPage(int cur_page, int alloc)
{
    PAGE_CTRL_INFO ctrl;
    int next;

    FlashRead(DATA_PAGE_START + cur_page, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
    next = ctrl.byte[0] * 256 + ctrl.byte[1];
    if (alloc && next == FS_NULL)
    {
        next = GetFreePage();
        if (next == -1)
            return FS_NULL; // no free page
        ctrl.byte[0] = next / 256;
        ctrl.byte[1] = next % 256;
        FlashWrite(DATA_PAGE_START + cur_page, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
    }

    return next;    
}

int Sys_FS_Write(FILE_DESCRIPTION *file, char *pData, int nLen)
{
    int offset = 0, total, write, unit, next;
    
    if (file->cur_page == -1)
    {
        //查找file->offset所在的块及偏移     
        file->cur_page = file->head_page;
        while (1)
        {
            unit = PAGE_DATA_SIZE;
            if (file->cur_page == file->head_page)
                unit -= sizeof (FILE_HEAD);
            if (offset + unit > file->offset)
            {
                file->cur_offset = file->offset - offset;
                break;
            }
            offset += unit;

            next = GetNextPage(file->cur_page, 1);
            if (next == FS_NULL)
                return -1; //跳转处空间不足
            file->cur_page = next;            
        }    
    }

    total = 0;
    while (1)
    {
        unit = PAGE_DATA_SIZE;
        if (file->cur_page == file->head_page)
            unit -= sizeof (FILE_HEAD);
        write = nLen - total;
        if (write > unit - file->cur_offset)
            write = unit - file->cur_offset;        

        offset = sizeof (PAGE_CTRL_INFO) + file->cur_offset;
        if (file->cur_page == file->head_page)
            offset += sizeof (FILE_HEAD);
        FlashWrite(DATA_PAGE_START + file->cur_page, offset, pData + total, write);
        file->offset += write;
        if (file->offset > file->cur_size)
            file->cur_size = file->offset;
        file->cur_offset += write;
        total += write;        
        
        if (total >= nLen)
            break;

        next = GetNextPage(file->cur_page, 1);
        if (next == FS_NULL)
            break; //空间不足
        file->cur_page = next;            
        file->cur_offset = 0;
    }

    return total;
}

int Sys_FS_Read(FILE_DESCRIPTION *file, char *pData, int nLen)
{
    int offset = 0, total, read, unit, next;
    
    if (file->offset >= file->cur_size)
        return 0;

    if (file->cur_page == -1)
    {
        //查找file->offset所在的块及偏移     
        file->cur_page = file->head_page;
        while (1)
        {
            unit = PAGE_DATA_SIZE;
            if (file->cur_page == file->head_page)
                unit -= sizeof (FILE_HEAD);
            if (offset + unit > file->offset)
            {
                file->cur_offset = file->offset - offset;
                break;
            }
            offset += unit;

            next = GetNextPage(file->cur_page, 0);
            if (next == FS_NULL)
                return -1; //bug
            file->cur_page = next;            
        }    
    }

    total = 0;
    while (1)
    {
        unit = PAGE_DATA_SIZE;
        if (file->cur_page == file->head_page)
            unit -= sizeof (FILE_HEAD);
        read = nLen - total;
        if (read > unit - file->cur_offset)
            read = unit - file->cur_offset;        
        if (file->cur_size - file->offset < read)
            read = file->cur_size - file->offset;

        offset = sizeof (PAGE_CTRL_INFO) + file->cur_offset;
        if (file->cur_page == file->head_page)
            offset += sizeof (FILE_HEAD);
        FlashRead(DATA_PAGE_START + file->cur_page, offset, pData + total, read);
        file->offset += read;
        file->cur_offset += read;
        total += read;
        
        if (total >= nLen || file->offset >= file->cur_size)
            break;

        next = GetNextPage(file->cur_page, 0);
        if (next == FS_NULL)
            return -1; //bug
        file->cur_page = next;            
        file->cur_offset = 0;
    }

    return total;
}

int Sys_FS_Close(FILE_DESCRIPTION *file)
{
    if (file->head_info.size == FS_NULL ||
        file->cur_size > file->head_info.size)
    {
        file->head_info.size = file->cur_size;
        FlashWrite(DATA_PAGE_START + file->head_page, sizeof (PAGE_CTRL_INFO), (char *) &file->head_info, sizeof (FILE_HEAD));
    }

    return 0;
}

int Sys_FS_Delete(char *pszName)
{
    FILE_DESCRIPTION file;
    PAGE_CTRL_INFO ctrl, tmp;
    HASH_NODE hash[MAX_HASH_BUCKET];
    int value, p;

    if (strlen(pszName) > 13)
        return -1;

    if (FindFile(pszName, &file) != 0)
        return -2;

    FlashRead(DATA_PAGE_START + file.head_page, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));

    //将文件首块从hash中删除
    if (file.pre_hash_page == FS_NULL) //hash链表第一个元素
    {
        if (GetHashIndex(hash) == -1)
            return -3; //bug
        value = GetHashValue(pszName);
        hash[value].page_no = ctrl.byte[2] * 256 + ctrl.byte[3];
        if (SetHashIndex(hash) == -1)
            return -4; //bug
    }
    else
    {
        FlashRead(DATA_PAGE_START + file.pre_hash_page, 0, (char *) &tmp, sizeof (PAGE_CTRL_INFO));
        tmp.byte[2] = ctrl.byte[2];
        tmp.byte[3] = ctrl.byte[3];
        FlashWrite(DATA_PAGE_START + file.pre_hash_page, 0, (char *) &tmp, sizeof (PAGE_CTRL_INFO));
    }

    //释放文件块
    p = file.head_page;
    while (p != FS_NULL)
    {
        FlashRead(DATA_PAGE_START + p, 0, (char *) &ctrl, sizeof (PAGE_CTRL_INFO));
        FlashErase(DATA_PAGE_START + p);
        SetRecyclePage(p);
        p = ctrl.byte[0] * 256 + ctrl.byte[1];
    }

    return 0;
}
