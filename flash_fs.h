#ifndef _FLASH_FS
#define _FLASH_FS

//实现一个简单有效的flash存储结构
//直接通过文件标识访问, 不含目录结构

//每扇区头8字节用做控制结构, 实际数据区为256字节

//扇区分配:
//hash索引扇区 节点定义为2字节扇区号(0xFF 0xFF为空), 支持32项的链式hash表, 共2个扇区中可存放8组
//bitmap扇区   最多256 * 8 = 2048 每位对应一个page, 1表示空闲, 0表示已分配, 共1个扇区
//待回收扇区   当文件块释放后, 不直接修改bitmap, 先擦除该块, 再将待回收扇区相应位置零, 等bitmap中分配不到
//             空闲块时, 将bitmap中对应待回收扇区中为0的位重置为1, 最后擦除待回收页(可大大减少bitmap区擦除
//             次数, 并可平均使用空闲页)
//坏块扇区     1 好块  0 坏块(预留, 暂不实现)
//文件扇区     文件首块存放FILE_HEAD占用16字节, 后240字节存储文件数据, 后面的文件块直接存放数据

//空闲页分配过程:
//1. 查找bitmap区中为1的位, 若找不到空闲块跳转2, 找到则将相应位置0后退出
//2. 根据待回收页回收空闲页, 再执行1的过程, 若仍找不到则分配失败

//查找文件过程:
//根据文件名计算hash值找到相应的槽, 遍历冲突链表找到相应文件

//添加文件过程:
//根据文件名计算hash值找到相应的槽, 分配文件块, 将文件首块插入在槽的首部

//修改文件过程:
//若是在尾部添加数据可以直接写操作, 否则需要先擦除页

//删除文件过程:

#define TAG_FREE       0xFF  //空闲
#define TAG_VALID      0xF0  //有效
#define TAG_INVALID    0x00  //无效

#define FS_FLAG_PAGE_NUM        1
#define HASH_NODE_PAGE_NUM      2
#define BITMAP_PAGE_NUM         1
#define RECYCLE_PAGE_NUM        1
#define BAD_PAGE_PAGE_NUM       1
#define DATA_PAGE_NUM           2000

#define FS_FLAG_PAGE_START      0//1000
#define HASH_NODE_PAGE_START    (FS_FLAG_PAGE_START + FS_FLAG_PAGE_NUM)
#define BITMAP_PAGE_START       (HASH_NODE_PAGE_START + HASH_NODE_PAGE_NUM)
#define RECYCLE_PAGE_START      (BITMAP_PAGE_START + BITMAP_PAGE_NUM)
#define BAD_PAGE_PAGE_START     (RECYCLE_PAGE_START + RECYCLE_PAGE_NUM)
#define DATA_PAGE_START         (BAD_PAGE_PAGE_START + BAD_PAGE_PAGE_NUM)

#define PAGE_DATA_SIZE          (FLASH_PAGE_SIZE - sizeof (PAGE_CTRL_INFO))

#define MAX_HASH_BUCKET         32
#define HASH_INDEX_SIZE         (MAX_HASH_BUCKET * sizeof (HASH_NODE))

#define FS_NULL                 0xFFFF

#define FS_SEEK_SET             0x1
#define FS_SEEK_CUR             0x2
#define FS_SEEK_END             0x3

#define FS_RDWR                 0x1 //若文件不存在则创建
#define FS_RDONLY               0x2 //若文件不存在则报错

typedef struct {
    unsigned char byte[8]; 
    //FS_FLAG扇区
    //byte0 ~ byte5   "FORMAT", 已格式化标志

    //索引扇区
    //扇区1
    //byte0           每位对应每组hash数组状态, 1 有效, 0 无效, 第一个1为当前索引

    //bitmap扇区
    //无定义

    //待回收扇区
    //无定义

    //文件扇区
    //首块
    //byte0 byte1     下一数据链接页号, 0xFF 0xFF表示末尾
    //byte2 byte3     下一具有相同hash值的文件链接页号, 0xFF 0xFF表示尾
    //后续块
    //byte0 byte1     下一数据链接页号, 0xFF 0xFF表示末尾
} PAGE_CTRL_INFO; //8bytes

typedef struct {
    unsigned short page_no; //链接扇区号
} HASH_NODE;

typedef struct {
    char name[14];       //文件名, '\0'结束, 最长13字节
    unsigned short size; //文件大小
} FILE_HEAD;  //16bytes

typedef struct {
    int pre_hash_page;   //相同hash的前一个文件(删除文件时需要修改它的next) 
    int head_page;       //文件首块
    int offset;          //当前文件偏移 
    int cur_page;        //当前文件偏移对应的文件块, 未初始化时为-1 
    int cur_offset;      //当前文件偏移对应的文件块页内偏移, 未初始化时为-1 
    int cur_size;        //当前文件大小, 只有FS_Write会改变文件大小, 且在FS_Close
                         //时才将head_info.size保存进flash
    FILE_HEAD head_info;  
} FILE_DESCRIPTION;

void Sys_InitFS(void);

void FormatFS(void);

int Sys_FS_Open(char *pszName, FILE_DESCRIPTION *file, int flag);

int Sys_FS_Write(FILE_DESCRIPTION *file, char *pData, int nLen);

int Sys_FS_Read(FILE_DESCRIPTION *file, char *pData, int nLen);

int Sys_FS_Close(FILE_DESCRIPTION *file);

int Sys_FS_Seek(FILE_DESCRIPTION *file, int offset, int origin);

int Sys_FS_Delete(char *pszName);

#endif

