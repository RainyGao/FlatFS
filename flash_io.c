#include <string.h>
#include "at45db041b.h"
#include "flash_io.h"

int FlashWrite(int PageNo, int Offset, char *pData, int nLen)
{
    char page[FLASH_PAGE_SIZE];
    int i; 
    
    if (PageNo < 0 || PageNo >= FLASH_PAGE_NUM)
        return -1;
    if (Offset < 0 || Offset >= FLASH_PAGE_SIZE)
        return -1;
    if (Offset + nLen > FLASH_PAGE_SIZE)
        return -1;

    FlashRead(PageNo, 0, page, FLASH_PAGE_SIZE);

    for (i = 0; i < nLen; ++ i)
    {
        page[Offset + i] &= pData[i];

        if (page[Offset + i] != pData[i]) //��Ҫ����ҳд
        {
            memcpy(page + Offset, pData, nLen);
            FlashErase(PageNo);    
            break;
        }
    }    

    AT45DB041B_WriteFlashNoErase(1, PageNo, 0, (unsigned char *)page, FLASH_PAGE_SIZE);
    
    return 0;
}

int FlashRead(int PageNo, int Offset, char *pData, int nLen)
{
   // int i;

    if (PageNo < 0 || PageNo >= FLASH_PAGE_NUM)
        return -1;
    if (Offset < 0 || Offset >= FLASH_PAGE_SIZE)
        return -1;
    if (Offset + nLen > FLASH_PAGE_SIZE)
        return -1;

    AT45DB041B_ReadFlashDirect(PageNo, Offset, (unsigned char *)pData, nLen);

    return 0;
}

int FlashErase(int PageNo)
{
    if (PageNo < 0 || PageNo >= FLASH_PAGE_NUM)
        return -1;

    AT45DB041B_ErasePage(PageNo);

    return 0;
}

