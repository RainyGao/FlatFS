#ifndef _AT45DB041B_H
#define _AT45DB041B_H

void AT45DB041B_ReadFlashDirect(int page, int offset, unsigned char *buf, int len);

void AT45DB041B_WriteFlashNoErase(int bid, int page, int offset, unsigned char *buf, int len);

void AT45DB041B_ErasePage(int page);

void AT45DB041B_WriteBuffer1(int offset, unsigned char *buf, int len);

void AT45DB041B_WriteBuffer2(int offset, unsigned char *buf, int len);

void AT45DB041B_ReadBuffer1(int offset, unsigned char *buf, int len);

void AT45DB041B_ReadBuffer2(int offset, unsigned char *buf, int len);

#endif
