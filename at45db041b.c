#include "PeripheralsConf.h"
#include "stm32f10x_lib.h"
#include "at45db041b.h"

static unsigned char SPI_ReadByte(unsigned char byte)
{
	/* 等待数据寄存器空 */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET) ;

	/* 通过SPI1接口发送数据 */
	SPI_I2S_SendData(SPI1, byte);

	/* 等待接收到一个字节的数据 */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET) ;

	/* 返回接收的数据 */
	return SPI_I2S_ReceiveData(SPI1);
}

static unsigned char ReadStatus(void)
{
    unsigned char byte;

    AT45DB041B_CS_LOW();
    SPI_ReadByte(0xD7);
    byte = SPI_ReadByte(0);
    AT45DB041B_CS_HIGH();

    return byte;
}

static void WaitIDLE(void)
{
    while (!(ReadStatus() & 0x80)) ;
}

static void WriteBuffer(int id, int offset, unsigned char *buf, int len)
{
    int i;
       
    WaitIDLE();
    
    AT45DB041B_CS_LOW() ;
    
    if (id == 1)
        SPI_ReadByte(0x84);
    else
        SPI_ReadByte(0x87);

    SPI_ReadByte(0x00);
    SPI_ReadByte(offset >> 8);
    SPI_ReadByte(offset);

    for (i = 0; i < len; ++ i)
        SPI_ReadByte(buf[i]);
    
    AT45DB041B_CS_HIGH();
}

void AT45DB041B_WriteBuffer1(int offset, unsigned char *buf, int len)
{
    WriteBuffer(1, offset, buf, len);
}

void AT45DB041B_WriteBuffer2(int offset, unsigned char *buf, int len)
{
    WriteBuffer(2, offset, buf, len);  
}

static void ReadBuffer(int id, int offset, unsigned char *buf, int len)
{
    int i;
       
    WaitIDLE();
    
    AT45DB041B_CS_LOW() ;
    
    if (id == 1)
        SPI_ReadByte(0x54);
    else
        SPI_ReadByte(0x56);

    SPI_ReadByte(0x00);
    SPI_ReadByte(offset >> 8);
    SPI_ReadByte(offset);
    SPI_ReadByte(0x00);    

    for (i = 0; i < len; ++ i)
        buf[i] = SPI_ReadByte(0x00);
    
    AT45DB041B_CS_HIGH();    
}

void AT45DB041B_ReadBuffer1(int offset, unsigned char *buf, int len)
{
    ReadBuffer(1, offset, buf, len);
}

void AT45DB041B_ReadBuffer2(int offset, unsigned char *buf, int len)
{
    ReadBuffer(2, offset, buf, len);
}

void AT45DB041B_ErasePage(int page)
{
    WaitIDLE();
    
    AT45DB041B_CS_LOW() ;
    
    SPI_ReadByte(0x81);
    SPI_ReadByte(page >> 7);
    SPI_ReadByte(page << 1);
    SPI_ReadByte(0x00);
    
    AT45DB041B_CS_HIGH();
}

void AT45DB041B_ReadFlashDirect(int page, int offset, unsigned char *buf, int len)
{
    int i;

    WaitIDLE();
    
    AT45DB041B_CS_LOW();
    
    SPI_ReadByte(0xE8);
    SPI_ReadByte(page >> 7);
    SPI_ReadByte((page << 1) | (offset >> 8));
    SPI_ReadByte(offset);
    
    for (i = 0; i < 4; ++ i)
        SPI_ReadByte(0x00);

    for (i = 0; i < len; ++ i)
        buf[i] = SPI_ReadByte(0x00);
    
    AT45DB041B_CS_HIGH() ;
}

void AT45DB041B_WriteFlashNoErase(int bid, int page, int offset, unsigned char *buf, int len)
{  
    if (len < 264)
    {
        WaitIDLE();
        
        AT45DB041B_CS_LOW();
        
        switch(bid)
        {
        case 1:
            SPI_ReadByte(0x53);
            break;
        case 2:
            SPI_ReadByte(0x55);
            break;
        }
        
        SPI_ReadByte(page >> 7);
        SPI_ReadByte(page << 1);
        SPI_ReadByte(0x00);
        
        AT45DB041B_CS_HIGH();
        
        WaitIDLE();
    }

    WriteBuffer(bid, offset, buf, len);
    WaitIDLE();

    AT45DB041B_CS_LOW();
    
    switch (bid)
    {
    case 1:
        SPI_ReadByte(0x88);
        break;
    case 2:
        SPI_ReadByte(0x89);
        break;
    }
    
    SPI_ReadByte(page >> 7);
    SPI_ReadByte(page << 1);
    SPI_ReadByte(0x00);
    
    AT45DB041B_CS_HIGH();
    
    WaitIDLE();
}
