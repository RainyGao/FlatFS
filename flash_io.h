#ifndef _FLASH_IO
#define _FLASH_IO

#define FLASH_PAGE_NUM    2048
#define FLASH_PAGE_SIZE   264
#define FLASH_SIZE        (FLASH_PAGE_NUM * FLASH_PAGE_SIZE)

int FlashWrite(int PageNo, int Offset, char *pData, int nLen);

int FlashRead(int PageNo, int Offset, char *pData, int nLen);

int FlashErase(int PageNo);

#endif

